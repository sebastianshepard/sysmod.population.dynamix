/**
 * Created by sovcharenko on 01.04.2016.
 */

pendulumApp.controller('chartCtrl', ['$scope', '$rootScope', function($scope, $rootScope) {

    this.drawChart = function() {

        var pendulum_damping_time = 0.0;
        var pendulum_damping_coef = parseFloat( ( parseFloat($scope.physics.resistance_coeff) / (2 * parseFloat($scope.physics.mass) ) ).toFixed(4) );

        var length_meters = parseFloat($scope.physics.length);
        var gravity_meters_per_sec_2 = parseFloat($scope.physics.gravity);
        var initialAngle_radian = parseFloat((parseFloat($scope.physics.initial_angle) * 0.0174533).toFixed(4));

        var timestep_miliseconds = 10;

        var is_damping = $scope.physics.is_damping;

        var prev = 0;

        $scope.physics_chart.dataY = [];
        $scope.physics_chart.dataX = [];
        $rootScope.results = [];

        var PendulumAlg = function(length_m, gravity_mps2, initialAngle_rad, timestep_ms) {
            var velocity = 0;
            var angle = initialAngle_rad;
            var prev_acc;
            var k = -gravity_mps2/length_m;
            var timestep_s = timestep_ms / 1000;
            var acceleration;

            var refresh = true;
            var refreshRev = true;

            $scope.physics_chart.time = parseFloat((pendulum_damping_time).toFixed(4));
            $scope.physics_chart.amplitude = parseFloat( ( Math.abs( length_m * Math.sin(angle) ) ).toFixed(4));
            $scope.physics_chart.angle = parseFloat((angle * 57.2958).toFixed(4));

            if(!is_damping) {

                pendulum_damping_time = $scope.physics_chart.accuracy_start;

                while($scope.physics_chart.time < $scope.physics_chart.accuracy_end) {
                    prev_acc = acceleration;

                    acceleration = k * Math.sin(angle);
                    velocity += acceleration * timestep_s;
                    pendulum_damping_time += timestep_s;
                    angle    += velocity * timestep_s;

                    if( prev_acc < acceleration && acceleration < 0 ) {
                        if(refresh) {

                            $scope.physics_chart.dataY.push($scope.physics_chart.amplitude);
                            $scope.physics_chart.dataX.push($scope.physics_chart.time + ' сек');

                            $rootScope.results.push({
                                time: $scope.physics_chart.time,
                                amplitude: $scope.physics_chart.amplitude * 2,
                                angle: $scope.physics_animation.angle
                            });

                            refresh = false;
                        }
                    }
                    if(acceleration > 0) refresh = true;

                    if( prev_acc > acceleration && acceleration > 0 ) {
                        if(refreshRev) {

                            $scope.physics_chart.dataY.push(-1 * $scope.physics_chart.amplitude);
                            $scope.physics_chart.dataX.push($scope.physics_chart.time + ' сек');

                            $rootScope.results.push({
                                time: $scope.physics_chart.time,
                                amplitude: $scope.physics_chart.amplitude * 2,
                                angle: $scope.physics_chart.angle
                            });

                            refreshRev = false;
                        }
                    }
                    if(acceleration < 0) refreshRev = true;

                    $scope.physics_chart.time = parseFloat((pendulum_damping_time).toFixed(4));

                }

            } else {

                pendulum_damping_time = 0.0;

                while($scope.physics_chart.amplitude > $scope.physics_chart.accuracy_damping) {
                    prev_acc = acceleration;

                    acceleration = k * Math.sin(angle);
                    velocity += acceleration * timestep_s;
                    pendulum_damping_time += timestep_s;
                    angle    += velocity * timestep_s;

                    if( prev_acc < acceleration && acceleration < 0 ) {
                        if(refresh) {

                            $scope.physics_chart.amplitude = parseFloat( ( Math.abs( length_m * Math.sin(angle * Math.exp( -1 * pendulum_damping_coef * pendulum_damping_time)) ) ).toFixed(4));
                            $scope.physics_chart.angle = parseFloat((angle * Math.exp( -1 * pendulum_damping_coef * pendulum_damping_time) * 57.2958).toFixed(4));

                            $scope.physics_chart.dataY.push($scope.physics_chart.amplitude);
                            $scope.physics_chart.dataX.push($scope.physics_chart.time + ' сек');

                            $rootScope.results.push({
                                time: $scope.physics_chart.time,
                                amplitude: $scope.physics_chart.amplitude * 2,
                                angle: $scope.physics_chart.angle
                            });

                            refresh = false;
                        }
                    }
                    if(acceleration > 0) refresh = true;

                    if( prev_acc > acceleration && acceleration > 0 ) {
                        if(refreshRev) {

                            $scope.physics_chart.dataY.push(-1 * $scope.physics_chart.amplitude);
                            $scope.physics_chart.dataX.push($scope.physics_chart.time + ' сек');

                            $rootScope.results.push({
                                time: $scope.physics_chart.time,
                                amplitude: $scope.physics_chart.amplitude * 2,
                                angle: $scope.physics_chart.angle
                            });

                            refreshRev = false;
                        }
                    }
                    if(acceleration < 0) refreshRev = true;

                    $scope.physics_chart.time = parseFloat((pendulum_damping_time).toFixed(4));

                }
            }

         };

        PendulumAlg(length_meters, gravity_meters_per_sec_2, initialAngle_radian, timestep_miliseconds);

        var ctx = document.getElementById("pendulum_chart").getContext("2d");

        var data = {
            labels: $scope.physics_chart.dataX,
            datasets: [
                {
                    label: "Math Pendulum",
                    fillColor: "rgba(220,220,220,0.2)",
                    strokeColor: "black",
                    pointColor: "black",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "green",
                    pointHighlightStroke: "#fff",
                    data: $scope.physics_chart.dataY
                }
            ]
        };

        var myLineChart = new Chart(ctx).Line(data);
    };

}]);