

'use strict';

var populationApp = angular.module('populationApp', []);

populationApp.controller('populationController', ['$scope', function( $scope ) {

    $scope.Math = window.Math; //для того чтобы можно было применить математический модуль в модулях

    $scope.population_params = {//параметры моделирования (по умолчанию)
        start_time: 0.0,//начальное время

        start_population: 1000.0,//стартовая популяция
        start_population_title: "Начальная популяция (целые числа)",
        start_population_min: 1,
        start_population_max: 100000.0,
        start_population_exclude: '.',

        start_resources: 72000.0,//стартовые ресурсы
        start_resources_title: "Ёмкость среды / кол. ресурсов",
        start_resources_min: 'start_population',
        start_resources_max: 10000000.0,

        step_time: 0.2,//шаг инкремента времени
        step_time_title: "Шаг времени моделирования",
        step_time_min: 0.1,
        step_time_max: 4,

        coeff_resources_decrement: 0.3,//потребление ресурсов
        coeff_resources_decrement_title: "Коеффициент потребления одной особи",
        coeff_resources_decrement_min: 0.01,
        coeff_resources_decrement_max: 1,

        coeff_born: 2.8,//бионический коеффициент (размножаемость)
        coeff_born_title: "Бионический коеффициент",
        coeff_born_min: 0.5,
        coeff_born_max: 10
    };

    $scope.chart_data = {};//тут будут данные для графика

    $scope.results = [];//туту будут данные для таблицы

    //ф-я содержащая алгоритм и отрисовку графика
    $scope.DrawChart = function() {

        //сбрасываем начальные переменные

        var buffer = {//буферная переменная со всеми даными вычислений, нужна для норм. работы алгоритма
            time: $scope.population_params.start_time,//текущее время
            resources: $scope.population_params.start_resources,//текущие ресурсы
            population: $scope.population_params.start_population,//текущая популяция
            delta: 0//текущий прирост
        };

        $scope.chart_data.dataX_time = [];//время для графика (ось Х)
        $scope.chart_data.dataY_current_population = [];// поточная поп. для графика (ось У)
        $scope.chart_data.dataY_current_resources = [];// поточные ресурсы для графика (ось У)
        $scope.chart_data.dataY_delta = [];//поточный прирост для графика (ось У)

        $scope.results = [];//результаты (для таблицы)

        //алгоритм
        do {

            buffer = { // формулы взяты из лаб.1 и доп. ресурсов об "логистической формуле"
                time: buffer.time,
                resources: (buffer.resources - buffer.population * $scope.population_params.coeff_resources_decrement) >= 0
                    ? (buffer.resources - buffer.population * $scope.population_params.coeff_resources_decrement) : 0.0 ,
                delta: ($scope.population_params.start_resources * $scope.population_params.start_population
                    * Math.exp( Math.sign(buffer.resources) * $scope.population_params.coeff_born * buffer.time) /
                    ($scope.population_params.start_resources + $scope.population_params.start_population
                    * (Math.exp( Math.sign(buffer.resources) * $scope.population_params.coeff_born * buffer.time) - 1))) - buffer.population,
                population: ($scope.population_params.start_resources * $scope.population_params.start_population
                    * Math.exp( Math.sign(buffer.resources) * $scope.population_params.coeff_born * buffer.time)) /
                    ($scope.population_params.start_resources + $scope.population_params.start_population
                    * (Math.exp( Math.sign(buffer.resources) * $scope.population_params.coeff_born * buffer.time) - 1))

            };

            $scope.results.push(buffer);//заносим результат в массив для таблицы

            //заносим результаты в массивы для графика
            $scope.chart_data.dataX_time.push(                  buffer.time.toFixed(2) );
            $scope.chart_data.dataY_current_population.push(    Math.round(buffer.population) );
            $scope.chart_data.dataY_current_resources.push(     buffer.resources.toFixed(2) );
            $scope.chart_data.dataY_delta.push(                 Math.round(buffer.delta) );

            buffer.time += $scope.population_params.step_time;//инкрементим время

        } while(buffer.resources > 0);//пока ресурсы не кончаться - моделируем рост

        //рисуем график
        $scope.chart = {//инициализируем переменную в модели чтоб хранить там график
            ctx: {},
            data: {},
            myLineChart: {}
        };

        $('#pendulum_chart').replaceWith( $('#pendulum_chart').get(0).outerHTML );//сбрасываем данные на канвасе

        $scope.chart.ctx = $('#pendulum_chart').get(0).getContext("2d");//получаем клнтекст канваса

        $scope.chart.data = {//формируем данные для библиотеки которая нам построит график
            labels: $scope.chart_data.dataX_time,
            datasets: [
                {
                    label: "Популяция",
                    fillColor: "rgba(220,220,220,0.2)",
                    strokeColor: "gray",
                    pointColor: "green",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "black",
                    pointHighlightStroke: "#fff",
                    data: $scope.chart_data.dataY_current_population
                },
                {
                    label: "Ресурсы",
                    fillColor: "rgba(220,220,220,0.2)",
                    strokeColor: "skyBlue",
                    pointColor: "blue",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "black",
                    pointHighlightStroke: "#fff",
                    data: $scope.chart_data.dataY_current_resources
                },
                {
                    label: "Прирост",
                    fillColor: "rgba(215, 40, 40, 0.1)",
                    strokeColor: "pink",
                    pointColor: "red",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "black",
                    pointHighlightStroke: "#fff",
                    data: $scope.chart_data.dataY_delta
                }
            ]
        };

        $scope.chart.myLineChart = new Chart($scope.chart.ctx).Line($scope.chart.data);//запускаем отрисовку графика

    };

    //валидация

    $scope.error = {
        isValid: true,
        title: "",
        min: 0.0,
        max: 0.0,
        curval: ""
    };

    $scope.validateParams = function() {
        for(var i in $scope.population_params) {
            if( //проверка на число
                typeof $scope.population_params[i] == 'undefined' ||
                $scope.population_params[i] == NaN ||

                (   //проверка минимума
                    (typeof $scope.population_params[ $scope.population_params[i + '_min'] ] != 'undefined'
                    && $scope.population_params[i] < $scope.population_params[$scope.population_params[i + '_min']])
                    ||
                    $scope.population_params[i] < $scope.population_params[i + '_min']
                ) ||

                (   //проверка максимума
                    (typeof $scope.population_params[ $scope.population_params[i + '_max'] ] != 'undefined'
                    && $scope.population_params[i] > $scope.population_params[$scope.population_params[i + '_max']])
                    ||
                    $scope.population_params[i] > $scope.population_params[i + '_max']
                ) ||

                    //проверка на содержание нежелательных символов
                (typeof $scope.population_params[i + '_exclude'] != 'undefined' &&
                $scope.population_params[i].toString().indexOf($scope.population_params[i + '_exclude']) != -1)
            ) {

                $scope.error = {
                    isValid: false,
                    title: $scope.population_params[i + '_title'],
                    min: $scope.population_params[i + '_min'],
                    max: $scope.population_params[i + '_max'],
                    curval: $scope.population_params[i]
                };
                return;
            }
        }
        $scope.error.isValid = true;
    };

    $scope.DrawChart();

}]);